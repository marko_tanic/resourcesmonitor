package com.bmu.markotanic.resourcemonitor.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.os.CpuUsageInfo;

import com.bmu.markotanic.resourcemonitor.db.model.CPUStat;

import java.sql.Timestamp;
import java.util.List;

@Dao
public interface CPUStatDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCPUStats(List<CPUStat> cpuStats);

    @Query("SELECT * FROM cpustat WHERE timestamp>:start and timestamp<:end")
    List<CPUStat> getCPUStatsForPeriod(Long start, Long end);

    @Query("SELECT * FROM cpustat WHERE cpuId=-1 ORDER BY timestamp DESC LIMIT :number")
    CPUStat[] getLastXTotalCPUStats(int number);

    @Query("DELETE FROM cpustat WHERE timestamp<:timestamp")
    void deleteOld(long timestamp);
}
