package com.bmu.markotanic.resourcemonitor.service;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.arch.persistence.room.Room;
import android.content.ComponentName;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.bmu.markotanic.resourcemonitor.db.MonitorDatabase;
import com.bmu.markotanic.resourcemonitor.monitors.CPUMonitor;
import com.bmu.markotanic.resourcemonitor.monitors.MemoryMonitor;
import com.bmu.markotanic.resourcemonitor.monitors.Monitor;

import java.util.ArrayList;

public class MonitorJobService extends JobService {

    private Thread samplingThread;
    private JobParameters jobParameters;
    private Runnable samplingRunnable = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void run() {
            runJob();
            jobFinished(jobParameters, true);
        }
    };

    public MonitorJobService() {
        Log.i("MonitorJobService", "Service created");
    }

    public static void schedule(JobScheduler scheduler) {
        JobInfo toSchedule = new JobInfo.Builder(1, new ComponentName("com.bmu.markotanic.resourcemonitor", "com.bmu.markotanic.resourcemonitor.service.MonitorJobService"))
                //.setPeriodic(10000, 1000)
                .setOverrideDeadline(300000)
                .build();
        scheduler.schedule(toSchedule);
    }

    private ArrayList<Monitor> monitors = new ArrayList<>();

    {
        monitors.add(new CPUMonitor());
        monitors.add(new MemoryMonitor());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void runJob() {
        MonitorDatabase db = Room.databaseBuilder(getApplicationContext(),
                MonitorDatabase.class, "monitordb").build();
        for (Monitor m : monitors) {
            m.monitorState(db, this);
        }
    }


    @Override
    public boolean onStartJob(JobParameters params) {
        jobParameters = params;
        samplingThread = new Thread(samplingRunnable);
        samplingThread.start();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (samplingThread.isAlive()) {
            samplingThread.interrupt();
        }
        return true;
    }
}
