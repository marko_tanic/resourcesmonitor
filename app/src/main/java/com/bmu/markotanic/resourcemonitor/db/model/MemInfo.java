package com.bmu.markotanic.resourcemonitor.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.lang.reflect.Field;

@Entity
public class MemInfo {

    public MemInfo(){}

    public MemInfo(String memInfoLine, Long timestamp) {
        String[] meminfos = memInfoLine.split("\n");
        this.timestamp = timestamp;
        memTotal = Long.parseLong(meminfos[0].split("\\s+")[1]);
        memFree = Long.parseLong(meminfos[1].split("\\s+")[1]);
        buffers = Long.parseLong(meminfos[2].split("\\s+")[1]);
        cached = Long.parseLong(meminfos[3].split("\\s+")[1]);
        swapCached = Long.parseLong(meminfos[4].split("\\s+")[1]);
        active = Long.parseLong(meminfos[5].split("\\s+")[1]);
        inactive = Long.parseLong(meminfos[6].split("\\s+")[1]);
        activeAnon = Long.parseLong(meminfos[7].split("\\s+")[1]);
        inactiveAnon = Long.parseLong(meminfos[8].split("\\s+")[1]);
        activeFile = Long.parseLong(meminfos[9].split("\\s+")[1]);
        inactiveFile = Long.parseLong(meminfos[10].split("\\s+")[1]);
        unevictable = Long.parseLong(meminfos[11].split("\\s+")[1]);
        mlocked = Long.parseLong(meminfos[12].split("\\s+")[1]);
        highTotal = Long.parseLong(meminfos[13].split("\\s+")[1]);
        highFree = Long.parseLong(meminfos[14].split("\\s+")[1]);
        lowTotal = Long.parseLong(meminfos[15].split("\\s+")[1]);
        lowFree = Long.parseLong(meminfos[16].split("\\s+")[1]);
        swapTotal = Long.parseLong(meminfos[17].split("\\s+")[1]);
        swapFree = Long.parseLong(meminfos[18].split("\\s+")[1]);
        dirty = Long.parseLong(meminfos[19].split("\\s+")[1]);
        writeback = Long.parseLong(meminfos[20].split("\\s+")[1]);
        anonPages = Long.parseLong(meminfos[21].split("\\s+")[1]);
        mapped = Long.parseLong(meminfos[22].split("\\s+")[1]);
        shmem = Long.parseLong(meminfos[23].split("\\s+")[1]);
        sReclaimable = Long.parseLong(meminfos[24].split("\\s+")[1]);
        sUnreclaim = Long.parseLong(meminfos[25].split("\\s+")[1]);
        kernelStack = Long.parseLong(meminfos[26].split("\\s+")[1]);
        pageTables = Long.parseLong(meminfos[27].split("\\s+")[1]);
        nfsUnstable = Long.parseLong(meminfos[28].split("\\s+")[1]);
        bounce = Long.parseLong(meminfos[29].split("\\s+")[1]);
        writebackTmp = Long.parseLong(meminfos[30].split("\\s+")[1]);
        commitLimit = Long.parseLong(meminfos[31].split("\\s+")[1]);
        committedAS = Long.parseLong(meminfos[32].split("\\s+")[1]);
        vmallocTotal = Long.parseLong(meminfos[33].split("\\s+")[1]);
        vmallocUsed = Long.parseLong(meminfos[34].split("\\s+")[1]);
        vmallocChunk = Long.parseLong(meminfos[35].split("\\s+")[1]);
    }

    @PrimaryKey(autoGenerate = true)
    public Integer id;

    public Long timestamp;

    @ColumnInfo(name = "memTotal")
    public long memTotal;
    @ColumnInfo(name = "memFree")
    public long memFree;
    @ColumnInfo(name = "buffers")
    public long buffers;
    @ColumnInfo(name = "cached")
    public long cached;
    @ColumnInfo(name = "swapCached")
    public long swapCached;
    @ColumnInfo(name = "active")
    public long active;
    @ColumnInfo(name = "inactive")
    public long inactive;
    @ColumnInfo(name = "activeAnon")
    public long activeAnon;
    @ColumnInfo(name = "inactiveAnon")
    public long inactiveAnon;
    @ColumnInfo(name = "activeFile")
    public long activeFile;
    @ColumnInfo(name = "inactiveFile")
    public long inactiveFile;
    @ColumnInfo(name = "unevictable")
    public long unevictable;
    @ColumnInfo(name = "mlocked")
    public long mlocked;
    @ColumnInfo(name = "highTotal")
    public long highTotal;
    @ColumnInfo(name = "highFree")
    public long highFree;
    @ColumnInfo(name = "lowTotal")
    public long lowTotal;
    @ColumnInfo(name = "lowFree")
    public long lowFree;
    @ColumnInfo(name = "swapTotal")
    public long swapTotal;
    @ColumnInfo(name = "swapFree")
    public long swapFree;

    @ColumnInfo(name = "dirty")
    public long dirty;
    @ColumnInfo(name = "writeback")
    public long writeback;
    @ColumnInfo(name = "anonPages")
    public long anonPages;
    @ColumnInfo(name = "mapped")
    public long mapped;
    @ColumnInfo(name = "shmem")
    public long shmem;
    @ColumnInfo(name = "slab")
    public long slab;
    @ColumnInfo(name = "sReclaimable")
    public long sReclaimable;
    @ColumnInfo(name = "sUnreclaim")
    public long sUnreclaim;
    @ColumnInfo(name = "kernelStack")
    public long kernelStack;
    @ColumnInfo(name = "pageTables")
    public long pageTables;
    @ColumnInfo(name = "nfsUnstable")
    public long nfsUnstable;
    @ColumnInfo(name = "bounce")
    public long bounce;
    @ColumnInfo(name = "writebackTmp")
    public long writebackTmp;
    @ColumnInfo(name = "commitLimit")
    public long commitLimit;
    @ColumnInfo(name = "committedAS")
    public long committedAS;
    @ColumnInfo(name = "vmallocTotal")
    public long vmallocTotal;
    @ColumnInfo(name = "vmallocUsed")
    public long vmallocUsed;
    @ColumnInfo(name = "vmallocChunk")
    public long vmallocChunk;

}
