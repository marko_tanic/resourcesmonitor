package com.bmu.markotanic.resourcemonitor.exceptions;

public class ResourceMonitorException extends Exception {
    public ResourceMonitorException(String message) {
        super(message);
    }
}
