package com.bmu.markotanic.resourcemonitor.Util;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.bmu.markotanic.resourcemonitor.exceptions.ResourceMonitorException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SuperuserReader {

    private static SuperuserReader instance;

    public static synchronized SuperuserReader getInstance() {
        if (instance == null) {
            instance = new SuperuserReader();
        }
        return instance;
    }

    private SuperuserReader() {}

    private Process suProcess;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public synchronized String read(String path) throws IOException, ResourceMonitorException {
        if (suProcess == null || !suProcess.isAlive()) {
            createSuProcess();
        }
        InputStream inputStream = suProcess.getInputStream();
        DataOutputStream outputStream = new DataOutputStream(suProcess.getOutputStream());
        outputStream.writeBytes(String.format("cat %s\n", path));
        outputStream.flush();

        Log.i("SuperUserReadService", "Preparing to read /proc/stat");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i("SuperUserReadService", "Input stream available chars: " + inputStream.available());

        StringBuilder sb = new StringBuilder();

        while (inputStream.available() > 0) {
            sb.append((char)inputStream.read());
        }
        return sb.toString();
    }

    private void createSuProcess() throws ResourceMonitorException {
        try {
            suProcess = Runtime.getRuntime().exec("su");
            Log.i("SuperUserReadService", "Got superuser privilegies");
        } catch (IOException e) {
            Log.e("SuperUserReadService", "Could not start su process", e);
            throw new ResourceMonitorException("Could not get superuser privilegies");
        }
    }

}
