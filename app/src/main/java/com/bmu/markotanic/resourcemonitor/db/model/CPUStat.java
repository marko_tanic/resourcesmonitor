package com.bmu.markotanic.resourcemonitor.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.bmu.markotanic.resourcemonitor.exceptions.ResourceMonitorException;


@Entity
public class CPUStat {
    @PrimaryKey(autoGenerate = true)
    public Integer id;

    public Long timestamp;

    @ColumnInfo(name = "cpuId")
    public Short cpuId;

    @ColumnInfo(name = "user")
    public Long user;

    @ColumnInfo(name = "nice")
    public Long nice;

    @ColumnInfo(name = "system")
    public Long system;

    @ColumnInfo(name = "idle")
    public Long idle;

    @ColumnInfo (name = "iowait")
    public Long iowait;

    @ColumnInfo (name = "irq")
    public Long irq;

    @ColumnInfo (name = "softirq")
    public Long softirq;

    public CPUStat() {}

    @RequiresApi(api = Build.VERSION_CODES.N)
    public CPUStat(String cpuLine, Long timestamp) {
        this.timestamp = timestamp;
        String[] cpuLines = cpuLine.split("\\s+");
//        StringBuilder sb = new StringBuilder();
//        Arrays.asList(cpuLines).stream().forEach(s -> {sb.append(s);sb.append(' ');});
//        Log.e("rawlines", sb.toString());
        if (cpuLines[0].length() > 3 && Character.isDigit(cpuLines[0].charAt(3))) {
//            Log.e("charat1", cpuLines[0].substring(3, 3));
//            Log.e("charat2", cpuLines[0].substring(3, 4));
            cpuId = Short.parseShort(cpuLines[0].substring(3, 4));
        } else {
            cpuId = -1;
        }
        user = Long.parseLong(cpuLines[1]);
        nice = Long.parseLong(cpuLines[2]);
        system = Long.parseLong(cpuLines[3]);
        idle = Long.parseLong(cpuLines[4]);
        iowait = Long.parseLong(cpuLines[5]);
        irq = Long.parseLong(cpuLines[6]);
        softirq = Long.parseLong(cpuLines[7]);
    }

    public CPUStat substract(CPUStat other) throws ResourceMonitorException {
        if (cpuId != other.cpuId) {
            throw new ResourceMonitorException("Cannot substract cpu stats for different cpus");
        }
        CPUStat ret = new CPUStat();
        ret.user = user - other.user;
        ret.nice = nice - other.nice;
        ret.system = system - other.system;
        ret.iowait = iowait - other.iowait;
        ret.idle = idle - other.idle;
        ret.irq = irq - other.irq;
        ret.softirq = softirq - other.softirq;
        return ret;
    }

    public Float getUsagePercentage() {
        float totalTime = user + nice + system + idle + iowait + irq + softirq;
        float usageTime = totalTime - idle - iowait;
        return usageTime / totalTime;
    }

    @Override
    public String toString() {
        return "CPUStat{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", cpuId=" + cpuId +
                ", user=" + user +
                ", nice=" + nice +
                ", system=" + system +
                ", idle=" + idle +
                ", iowait=" + iowait +
                ", irq=" + irq +
                ", softirq=" + softirq +
                '}';
    }
}
