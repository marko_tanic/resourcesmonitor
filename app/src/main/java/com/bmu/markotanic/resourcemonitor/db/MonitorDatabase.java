package com.bmu.markotanic.resourcemonitor.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.bmu.markotanic.resourcemonitor.db.dao.CPUStatDao;
import com.bmu.markotanic.resourcemonitor.db.dao.MemInfoDao;
import com.bmu.markotanic.resourcemonitor.db.model.CPUStat;
import com.bmu.markotanic.resourcemonitor.db.model.MemInfo;

@Database(
        entities = {
                CPUStat.class,
                MemInfo.class
        },
        version = 1
)
public abstract class MonitorDatabase extends RoomDatabase {
    public abstract CPUStatDao getCPUStatDAO();
    public abstract MemInfoDao getMemInfoDAO();
}