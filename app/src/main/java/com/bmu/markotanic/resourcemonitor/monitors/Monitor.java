package com.bmu.markotanic.resourcemonitor.monitors;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.bmu.markotanic.resourcemonitor.db.MonitorDatabase;

public interface Monitor {

    @RequiresApi(api = Build.VERSION_CODES.O)
    void monitorState(MonitorDatabase db, Context context);

}
