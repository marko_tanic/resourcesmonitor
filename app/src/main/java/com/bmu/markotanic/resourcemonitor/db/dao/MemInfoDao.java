package com.bmu.markotanic.resourcemonitor.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.bmu.markotanic.resourcemonitor.db.model.CPUStat;
import com.bmu.markotanic.resourcemonitor.db.model.MemInfo;

import java.util.List;

@Dao
public interface MemInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMemInfo(List<MemInfo> cpuStats);

    //@Query("SELECT * FROM meminfo WHERE timestamp>:start and timestamp<:end")
    //List<CPUStat> getMemInfoForPeriod(Long start, Long end);
//
    //@Query("SELECT * FROM cpustat WHERE cpuId=-1 ORDER BY timestamp DESC LIMIT :number")
    //MemInfo[] getLastXTotalCPUStats(int number);
//
    //@Query("DELETE FROM cpustat WHERE timestamp<:timestamp")
    //void deleteOld(long timestamp);
}
