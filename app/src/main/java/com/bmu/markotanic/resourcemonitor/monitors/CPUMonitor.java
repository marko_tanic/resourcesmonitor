package com.bmu.markotanic.resourcemonitor.monitors;

import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.bmu.markotanic.resourcemonitor.db.MonitorDatabase;
import com.bmu.markotanic.resourcemonitor.db.model.CPUStat;
import com.bmu.markotanic.resourcemonitor.exceptions.ResourceMonitorException;
import com.bmu.markotanic.resourcemonitor.Util.SuperuserReader;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class CPUMonitor implements Monitor {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void monitorState(MonitorDatabase db, Context context) {
        db.getCPUStatDAO().deleteOld(System.currentTimeMillis() - SystemClock.elapsedRealtime());
        try {
            String rawProcStat = SuperuserReader.getInstance().read("/proc/stat");
            Log.i("Raw proc stat", rawProcStat);
            String[] procStats = rawProcStat.split("\n");
            List<CPUStat> cpuStats = new ArrayList<>(5);
            Long timestamp = System.currentTimeMillis();
            for (String procStat : procStats) {
                if (procStat.startsWith("cpu")) {
                    // total cpu activity
                    cpuStats.add(new CPUStat(procStat, timestamp));
                }
            }
            db.getCPUStatDAO().insertCPUStats(cpuStats);
        } catch (ResourceMonitorException | IOException e) {
            Log.e("MonitorJobService", "Could not read /proc/stat");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        examineLastCpuStats(db, context);
    }

    /**
     * Business logic. Examine last 3 reads, and see if the CPU usage has crossed a certain trheshold
     * Display warning if it has
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void examineLastCpuStats(MonitorDatabase db, Context context) {
        // Examine last 3 periods
        CPUStat[] last4Stats = db.getCPUStatDAO().getLastXTotalCPUStats(4);
        if (last4Stats.length < 4) {
            Log.i("MonitorJobService", String.format("Not enough records in db. Num of records: %s", last4Stats.length));
            return;
        }
        for (int i = 0; i < 4; ++i) {
            Log.i("CpuStat", last4Stats[i].toString());
        }
        CPUStat cutoff = findCutoffStat(last4Stats);
        if (cutoff != null) {
            Log.i("MonitorJobService", String.format("Cutoff detected : %s", cutoff.timestamp));
            db.getCPUStatDAO().deleteOld(cutoff.timestamp);
            return;
        }


        List<CPUStat> diffs = new ArrayList<>(3);
        try {
            for (int i = 0; i < 3; ++i) {
                diffs.add(last4Stats[i].substract(last4Stats[i + 1]));
            }
        } catch (ResourceMonitorException e) {
            Log.e("MonitorJobService", "Tried substracting different cpus");
            return;
        }
        diffs.stream().forEach(diff -> Log.i("Diff", diff.toString()));
        Float totalPercentage = diffs.stream().map(diff -> diff.getUsagePercentage()).reduce((accumulator, diff) -> accumulator + diff).get();
        totalPercentage /= diffs.size();
        Log.i("CPUMonitor", totalPercentage.toString());
        if (totalPercentage > 0.5f) {
            // Notification: high cpu usage
            NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context, "resMonChann")
                    .setContentTitle("Resource Monitor Alert")
                    .setContentText("Warning - high cpu usage detected")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSmallIcon(android.support.compat.R.drawable.notification_template_icon_bg);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(new Random(System.currentTimeMillis()).nextInt(), notifBuilder.build());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private CPUStat findCutoffStat(CPUStat[] stats) {
        boolean valid = true;
        for (int i = 1; i < stats.length ; ++i) {
            CPUStat previous = stats[i];
            CPUStat current = stats[i - 1];
            valid &=
                    previous.system <= current.system &&
                    previous.softirq <= current.softirq &&
                    previous.iowait <= current.iowait &&
                    previous.nice <= current.nice &&
                    previous.irq <= current.irq &&
                    previous.idle <= current.idle;
            if (!valid) {
                return current;
            }
        }
        return null;
    }
}
