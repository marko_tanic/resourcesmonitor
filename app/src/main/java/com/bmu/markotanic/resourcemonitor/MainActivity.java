package com.bmu.markotanic.resourcemonitor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.bmu.markotanic.resourcemonitor.service.helper.MemoryLoadTestService;
import com.bmu.markotanic.resourcemonitor.service.MonitorJobService;

public class MainActivity extends AppCompatActivity {

    @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "resourceMonitorChannel";
            String description = "Channel for resource monitor notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("resMonChann", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        JobScheduler scheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduleJobService(scheduler);
    }

    private void scheduleJobService(JobScheduler scheduler) {
        MonitorJobService.schedule(scheduler);
    }

    public void memoryTest(View view) {
        Intent serviceIntent = new Intent(this, MemoryLoadTestService.class);
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");

        ContextCompat.startForegroundService(this, serviceIntent);
    }

}
