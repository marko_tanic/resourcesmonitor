package com.bmu.markotanic.resourcemonitor.monitors;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.bmu.markotanic.resourcemonitor.db.MonitorDatabase;
import com.bmu.markotanic.resourcemonitor.db.model.MemInfo;
import com.bmu.markotanic.resourcemonitor.exceptions.ResourceMonitorException;
import com.bmu.markotanic.resourcemonitor.Util.SuperuserReader;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class MemoryMonitor implements Monitor {
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void monitorState(MonitorDatabase db, Context context) {
        try {
            MemInfo mem = getCurrentMemInfo();

            db.getMemInfoDAO().insertMemInfo(Arrays.asList(mem));
            Double memFree = (mem.memFree + mem.cached) / (double)mem.memTotal;
            Log.i("MemoryMonitor", memFree.toString());
            if (memFree < 0.3) {
                NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context, "resMonChann")
                        .setContentTitle("Resource Monitor Alert")
                        .setContentText("Warning - low memory detected")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setSmallIcon(android.support.compat.R.drawable.notification_template_icon_bg);
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                notificationManager.notify(new Random(System.currentTimeMillis()).nextInt(), notifBuilder.build());
            }

        } catch (IOException | ResourceMonitorException e) {
            Log.e("MemoryMonitor", "Cannot read /proc/meminfo");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    public static MemInfo getCurrentMemInfo() throws IOException, ResourceMonitorException {
         String rawMemInfo = SuperuserReader.getInstance().read("/proc/meminfo");
         return new MemInfo(rawMemInfo, System.currentTimeMillis());
    }

}
