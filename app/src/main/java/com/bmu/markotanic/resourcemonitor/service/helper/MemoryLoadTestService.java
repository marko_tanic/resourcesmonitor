package com.bmu.markotanic.resourcemonitor.service.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bmu.markotanic.resourcemonitor.MainActivity;
import com.bmu.markotanic.resourcemonitor.R;
import com.bmu.markotanic.resourcemonitor.db.model.MemInfo;
import com.bmu.markotanic.resourcemonitor.exceptions.ResourceMonitorException;
import com.bmu.markotanic.resourcemonitor.monitors.MemoryMonitor;

import java.io.IOException;

public class MemoryLoadTestService extends Service {

    private static class AntiGCHelper {
        public AntiGCHelper next;
        private long value0;
        private long value1;
        private long value2;
        private long value3;
        private long value4;
        private long value5;
        private long value6;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    Thread t;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //if (true) return START_NOT_STICKY;
        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, "Memory load testing")
                .setContentTitle("Foreground Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(35467, notification);

        //do heavy work on a background thread

        t = new Thread(() -> {
            AntiGCHelper start = new AntiGCHelper();
            AntiGCHelper last = start;
            while (true) {
                MemInfo availableMemory;
                try {
                    availableMemory = MemoryMonitor.getCurrentMemInfo();
                } catch (IOException | ResourceMonitorException e) {
                    Log.e("MemoryLoadTestService", "Cannot read /proc/meminfo", e);
                    continue;
                }
                long freeMemoryBytes = (availableMemory.memFree + availableMemory.cached) * 1024;
                long nodesToCreate = freeMemoryBytes / 72;
                for (long i = 0; i < nodesToCreate; ++i) {
                    AntiGCHelper newNode = new AntiGCHelper();
                    last.next = newNode;
                    last = newNode;
                    start.value1 = i;
                    last.value2 = start.value1;
                }
            }
        });
        t.start();

        //stopSelf();

        return START_NOT_STICKY;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    "Memory load testing",
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
